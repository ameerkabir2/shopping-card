import {
  BASKET_LOADED,
  BASKET_CREAT,
  BASKET_ADD_ITEM
} from "../actions/basket";

const initialState = { items: {} };

function basketReducer(state = initialState, action) {
  console.log(action.type);
  switch (action.type) {
    case BASKET_ADD_ITEM:
      return {
        ...state,
        name: action.name,
        id: action.cartId,
        items: action.cartItems
      };
    case BASKET_LOADED:
      return {
        ...state,
        id: action.cartId,
        items: action.cartItems
      };
    case BASKET_CREAT:
      return {
        ...state,
        id: action.cartId,
        items: action.cartItems
      };
    default:
      return state;
  }
}

export default basketReducer;
