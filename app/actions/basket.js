export const BASKET_ADD_ITEM = "BASKET_ADD_ITEM";
export const BASKET_LOADING = "BASKET_LOADING";
export const BASKET_LOADED = "BASKET_LOADED";
export const BASKET_CREATE = "BASKET_CREATE";

export function loadBasket() {
  return async (dispatch, _, config) => {
    dispatch({ type: BASKET_LOADING });

    const response = await fetch(config.cartApi, {
      method: "POST",
      body: JSON.stringify({})
    });
    const basket = await response.json();

    dispatch({
      type: BASKET_LOADED,
      ...basket
    });
    // debugger;
  };
}
export function createBaskt() {
  return async (dispatch, _, config) => {
    const response = await fetch(config.cartApi, {
      method: "POST",
      body: JSON.stringify({})
    });
    const basket = await response.json();

    dispatch({
      type: BASKET_CREATE,
      ...basket
    });
  };
}

export function addToBasket(basketId, itemId, itemName) {
   debugger;
  return async (dispatch, _, config) => {
    console.log("hello from inner");
    const response = await fetch(
      `${config.cartApi}/${basketId}/{item}/${itemId}`,
      {
        method: "POST",
        body: JSON.stringify({
          quantity: 1
        })
      }
    );
    const basket = await response.json();
    console.log("firing add action");
    dispatch({
      type: BASKET_ADD_ITEM,
      ...basket
    });
  };
}
