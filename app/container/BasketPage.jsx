import React, { Component } from "react";
import { connect } from "react-redux";

import { ProductList } from "../components";

import * as basketActions from "../actions/basket";

export class BasketPage extends Component {
  constructor(props) {
    console.log(props);
    super(props);
    props.loadBasket();
  }
  componentDidMount() {
    this.props.addToBasket;
  }
  renderBasket() {
    const { basket } = this.props;
      debugger;
    if (basket && basket.items && Array.isArray(basket.items)) {
      return (
        
        basket.items.map(item => {
          return <div>{item.itemId}</div>;
        })
      );
    }
  }
  render() {
    const { isLoading, products, basket } = this.props;
    if (isLoading) {
      return <div> Loading your basket </div>;
    }

    return (
      <div className="container">
        <header className="mt-5 mb-5">
          <h1>Shopping Basket</h1>
        </header>
        <main className="row">
          <section className="col">
            <ProductList
              products={products}
              basket={basket}
              addToBasket={this.props.addToBasket}
            />
          </section>
          <section className="col">{/* Basket */}
                    {this.renderBasket()}

          </section>
        </main>
      </div>
    );
  }
}

export function mapStateToProps({ basket, products }) {
  // console.log({basket})
  return {
    isLoading: !basket.id,
    products,
    basket
  };
}

export default connect(
  mapStateToProps,
  basketActions
)(BasketPage);
